# Aureso Test

http://hr-manger.herokuapp.com/aureso_welcome/e816b75cb38b9186ac57e1ff54102094

### How to Configure

```sh
$ bundle install
$ bundle rake db:create
$ bundle rake db:migrate
```

### How To Test

```sh
$ bundle rake db:test:prepare
$ bundle exec rspec spec
```

### How To Run

```sh
$ bundle exec rails s
```




