class AddDiscountValueToOrder < ActiveRecord::Migration
  def change
    add_money :orders, :discount_value
  end
end
