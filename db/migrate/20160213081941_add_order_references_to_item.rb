class AddOrderReferencesToItem < ActiveRecord::Migration
  def change
    add_column :items, :item, :string
    add_reference :items, :order, index: true
    add_foreign_key :items, :orders
  end
end
