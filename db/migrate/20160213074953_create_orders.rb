class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :order_id, index: true
      t.string :email
      t.string :payment_method
      t.money :total_amount_net
      t.money :shipping_costs

      t.timestamps null: false
    end
  end
end
