module Api
  class OrdersController < ::ApplicationController
    def create
      @order = Order.new(order_params)
      if @order.save
        render json: @order
      else
        render json: { error: @order.errors }, status: 400
      end
    end

    private

    def order_params
      accessible = [ :order_id, :email, :payment_method, :shipping_costs, :total_amount_net,
                     items_attributes: [ :name, :qnt, :value, :category, :subcategory, :collection_id, { tag_list: [] } ] ]
      params.require(:order).permit(accessible)
    end
  end
end
