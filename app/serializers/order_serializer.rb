class OrderSerializer < ActiveModel::Serializer
  attributes :order_id, :email, :payment_method, :total_amount_net, :shipping_costs
  has_many :items

  def total_amount_net
    object.total_amount_net.to_s
  end

  def shipping_costs
    object.shipping_costs.to_s
  end
end
