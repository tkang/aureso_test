class ItemSerializer < ActiveModel::Serializer
  attributes :name, :qnt, :value, :category, :subcategory, :tags
end
