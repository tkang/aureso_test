module DiscountPolicyChecker
  def self.is_prestige_policy?(order)
    order.total_amount_net.to_f > 149.0
  end

  DISCOUNT_ITEM_IDS = []

  def self.is_discount_order?(order)
    DISCOUNT_ITEM_IDS.include?(order.items.map(&:id))
  end
end
