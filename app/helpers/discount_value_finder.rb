require 'nokogiri'
require 'open-uri'

module DiscountValueFinder

  def self.calculate_discount_value(order)
    return 0 unless DiscountPolicyChecker.is_discount_order?(order)
    v = DiscountPolicyChecker.is_prestige_policy?(order) ? find_prestige : find_fixed
    max = MaxDiscountChecker.max_discount(order)
    (v > max) ? max : v
  end

  private

  def self.find_fixed
    # `discount_value` is equal, how many words 'status' can you fnd on site https://developer.github.com/v3/#http-redirects
    url = "https://developer.github.com/v3/#http-redirects"
    Nokogiri::HTML(open(url)).at('body').inner_text.scan('status').count
  end

  def self.find_prestige
    # `discount_value` is equal, how many pubDate elements can you fnd on page http://www.yourlocalguardian.co.uk/sport/rugby/rss/
    # NOTE : I used pubdate as elem key because there is no element found with pubDate
    url = "http://www.yourlocalguardian.co.uk/sport/rugby/rss/"
    Nokogiri::HTML(open(url)).search('pubdate').count
  end

end
