module MaxDiscountChecker
  def self.max_discount(order)
    order.total_value * 0.25
  end
end
