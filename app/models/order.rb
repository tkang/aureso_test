class Order < ActiveRecord::Base
  validates :order_id, presence: true
  validates :order_id, uniqueness: true

  monetize :total_amount_net_cents, as: "total_amount_net", numericality: { greater_than_or_equal_to: 0 }
  monetize :shipping_costs_cents, as: "shipping_costs", numericality: { greater_than_or_equal_to: 0 }
  monetize :discount_value_cents, as: "discount_value", numericality: { greater_than_or_equal_to: 0 }

  has_many :items
  accepts_nested_attributes_for :items, reject_if: :all_blank, allow_destroy: true

  before_save :set_discount_value

  def total_value
    items.map(&:value).sum
  end

  private

  def set_discount_value
    self.discount_value = Money.new(DiscountValueFinder.calculate_discount_value(self))
  end

end
