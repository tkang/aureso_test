Rails.application.routes.draw do
  root :to => "home#index"

  namespace :api do
    scope "v:api_version", :api_version => /[_0-9]+/ do
      resources :orders, only: [ :create ]
    end
  end
end
