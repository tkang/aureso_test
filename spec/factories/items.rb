FactoryGirl.define do
  factory :item do
    name "MyString"
    qnt 1
    value 10
    category "category"
    subcategory "sub_category"
    collection_id 1

    factory :item_with_tag do
      after(:build) { |i| i.tag_list.add([*('A'..'Z')].sample(4).join) }
    end

    factory :item_with_multi_tags do
      after(:build) { |i| 2.times { i.tag_list.add([*('A'..'Z')].sample(4).join) } }
    end

  end
end
