FactoryGirl.define do
  factory :order do
    order_id { Order.last.try(:id).to_i + 1 }
    email { Forgery(:internet).email_address }
    payment_method "Visa"

    factory :order_with_item do
      after(:create) { |order| FactoryGirl.create(:item, order: order) }
    end

    factory :order_with_tagged_item do
      after(:create) { |order| FactoryGirl.create(:item_with_tag, order: order) }
    end

    factory :order_with_multi_items do
      after(:create) { |order| 2.times { FactoryGirl.create(:item, order: order) } }
    end

    factory :order_with_multi_tagged_items do
      after(:create) { |order| 2.times { FactoryGirl.create(:item_with_multi_tags, order: order) } }
    end
  end
end
