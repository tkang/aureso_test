require 'rails_helper'

describe Api::OrdersController do
  it { { post: '/api/v1/orders' }.should route_to(controller: "api/orders", action: "create", api_version: '1') }
end
