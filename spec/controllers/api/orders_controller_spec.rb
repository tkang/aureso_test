require 'rails_helper'

describe Api::OrdersController, "POST create" do
  # ASSUMPTION : I used items_attributes instead of items to be able to untilize 'accepts_nested_attributes_for' in order model
  let(:order_attrs) {
    FactoryGirl.attributes_for(:order).merge!(shipping_costs: "29.00",
                                              total_amount_net: "1898.00",
                                              items_attributes: [ FactoryGirl.attributes_for(:item).merge!(tag_list: [ 'porsche', 'design' ]) ] )
  }

  it { expect { post :create, api_version: 1, order: order_attrs }.to change(Order, :count).by(1) }

  it do
    post :create, api_version: 1, order: order_attrs, format: 'json'
    response.status.should == 200
    h = JSON.parse(response.body)
    h.symbolize_keys.keys.should =~ [ :order_id, :email, :payment_method, :total_amount_net, :shipping_costs, :items ]
  end

  it do
    order_attrs.delete(:order_id)
    post :create, api_version: 1, order: order_attrs, format: 'json'
    response.status.should == 400
    h = JSON.parse(response.body)
    h.symbolize_keys.keys.should =~ [ :error ]
  end
end
