require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to validate_presence_of(:order_id) }
  it { is_expected.to validate_uniqueness_of(:order_id) }
  it { is_expected.to monetize(:total_amount_net_cents) }
  it { is_expected.to monetize(:shipping_costs_cents) }
  it { is_expected.to have_many(:items) }
end


