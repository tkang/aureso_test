require 'rails_helper'

RSpec.describe MaxDiscountChecker, "max_discount" do
  let(:order) { FactoryGirl.create(:order_with_multi_items) }
  it do
    MaxDiscountChecker.max_discount(order).should == order.total_value * 0.25
  end
end

