require 'rails_helper'

RSpec.describe DiscountPolicyChecker, "is_prestige_policy?" do
  let(:order_with_more_than_149_total_amount_net) { FactoryGirl.build(:order, total_amount_net: 149.1) }
  let(:order_with_149_total_amount_net) { FactoryGirl.build(:order, total_amount_net: 149.0) }
  let(:order_with_less_than_149_total_amount_net) { FactoryGirl.build(:order, total_amount_net: 148.99) }

  it { DiscountPolicyChecker.is_prestige_policy?(order_with_more_than_149_total_amount_net).should == true }
  it { DiscountPolicyChecker.is_prestige_policy?(order_with_149_total_amount_net).should == false }
  it { DiscountPolicyChecker.is_prestige_policy?(order_with_less_than_149_total_amount_net).should == false }
end

RSpec.describe DiscountPolicyChecker, "is_discount_order?" do
  let(:order) { FactoryGirl.create(:order) }

  it { DiscountPolicyChecker.is_discount_order?(order).should == false }

  context "if order's items' ids are in discount_item_ids" do
    before { stub_const("DiscountPolicyChecker::DISCOUNT_ITEM_IDS", [ order.items.map(&:id) ]) }
    it { DiscountPolicyChecker.is_discount_order?(order).should == true }
  end
end
