require 'rails_helper'

RSpec.describe DiscountPolicyChecker, "calculate_discount_value" do
  let(:order) { FactoryGirl.create(:order_with_multi_items) }
  before do
    DiscountValueFinder.stub(:find_prestige) { 888 }
    DiscountValueFinder.stub(:find_fixed) { 999 }
    MaxDiscountChecker.stub(:max_discount).with(order) { 100000 }
  end

  context "if discount order" do
    before { DiscountPolicyChecker.stub(:is_discount_order?).with(order) { true } }
    context "if prestige policy?" do
      before { DiscountPolicyChecker.stub(:is_prestige_policy?).with(order) { true } }

      it "discount value should be equal to fixed discount" do
        DiscountValueFinder.calculate_discount_value(order).should == 888
      end
    end

    context "if not prestige policy?" do
      before { DiscountPolicyChecker.stub(:is_prestige_policy?).with(order) { false } }

      it "discount value should be equal to fixed discount" do
        DiscountValueFinder.calculate_discount_value(order).should == 999
      end
    end

    context "if greater than max discount" do
      before { MaxDiscountChecker.stub(:max_discount).with(order) { 0.1 } }
      it { DiscountValueFinder.calculate_discount_value(order).should == 0.1 }
    end
  end

  context "if not discount order" do
    before { DiscountPolicyChecker.stub(:is_discount_order?).with(order) { false } }
    it { DiscountValueFinder.calculate_discount_value(order).should == 0 }
  end
end
